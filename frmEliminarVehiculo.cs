﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProyectoDeCatedra_POO
{
    public partial class frmEliminarVehiculo : Form
    {
        
        public frmEliminarVehiculo()
        {
            InitializeComponent();
        }

        private void lboxVehiculos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lboxVehiculos_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void lboxVehiculos_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.lboxVehiculos.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                DialogResult dialogResult = MessageBox.Show("Esta seguro de eliminar el vehiculo seleccionado?", "Confirmacion", MessageBoxButtons.YesNo);
                if(dialogResult == DialogResult.Yes)
                {

                    string idVehiculo = lboxVehiculos.Text.ToString().Split("-")[0];
                    SqlCommand comando = new SqlCommand("UPDATE vehiculo set disponible=0 where id=" + idVehiculo, BDD.conexion);
                    BDD.open();
                    int i = comando.ExecuteNonQuery();
                    if (i != 0)
                    {
                        MessageBox.Show("Vehiculo eliminado con exito");
                        BDD.close();
                        fillListBox();
                    }
                    else
                    {
                        MessageBox.Show("Error en el borrado. Contacte al administrador", "Error");
                BDD.close();

                    }
                    BDD.close();
                }
            }
        }

        private void frmEliminarVehiculo_Load(object sender, EventArgs e)
        {

            fillListBox();
        }

        private void fillListBox()
        {
            BDD.open();
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(
            "SELECT CONCAT(id,'-',marca,'-',modelo,'-',anio,'-$',precio,'-',stock) as carro from vehiculo where disponible=1", BDD.conexion);
            adapter.Fill(ds);
            this.lboxVehiculos.DataSource = ds.Tables[0];
            this.lboxVehiculos.DisplayMember = "carro";
            BDD.close();
        }

        private void frmEliminarVehiculo_Load_1(object sender, EventArgs e)
        {
            fillListBox();
        }
    }
}