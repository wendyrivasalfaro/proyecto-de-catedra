﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProyectoDeCatedra_POO
{
    public partial class frmAgregarVehiculo : Form
    {
        public frmAgregarVehiculo()
        {
            InitializeComponent();
        }

        private void frmAgregarVehiculo_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtAnio.Text) || String.IsNullOrWhiteSpace(txtPrecio.Text) || String.IsNullOrWhiteSpace(txtUrl.Text) || String.IsNullOrWhiteSpace(cboxMarca.Text) || String.IsNullOrWhiteSpace(txtModelo.Text) || String.IsNullOrWhiteSpace(nudUnidades.Value.ToString()))
            {
                MessageBox.Show("Ingrese todos los datos", "Error");
            }
            else
            {
                SqlCommand comando = new SqlCommand("INSERT INTO vehiculo VALUES (@MARCA,@MODELO,@ANIO,@PRECIO,@STOCK,@URL,1)", BDD.conexion);
                comando.Parameters.AddWithValue("@MARCA", cboxMarca.Text);
                comando.Parameters.AddWithValue("@MODELO", txtModelo.Text);
                comando.Parameters.AddWithValue("@ANIO", txtAnio.Text);
                comando.Parameters.AddWithValue("@PRECIO", txtPrecio.Text);
                comando.Parameters.AddWithValue("@STOCK", nudUnidades.Value.ToString());
                comando.Parameters.AddWithValue("@URL", txtUrl.Text);
                BDD.open();
                int i = comando.ExecuteNonQuery();
                if (i != 0)
                {
                    MessageBox.Show("Vehiculo registrado con exito");
                    BDD.close();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Error en el registro. Contacte al administrador", "Error");
                    BDD.close();

                }
                BDD.close();
            }
        }
    }
}