CREATE DATABASE todoautos
GO
USE todoautos
GO

CREATE TABLE usuario(
id int PRIMARY KEY NOT NULL IDENTITY(1,1),
usuario varchar(255) NOT NULL,
nombre varchar(255) NOT NULL,
password varchar(255) NOT NULL,
tipo varchar(50) NOT NULL
)
GO

CREATE TABLE vehiculo(
id int PRIMARY KEY NOT NULL IDENTITY(1,1),
marca varchar(255) NOT NULL,
modelo varchar(255) NOT NULL,
anio int NOT NULL,
precio float NOT NULL,
stock int NOT NULL,
url varchar(MAX) NOT NULL
)
GO

CREATE TABLE venta(
id int PRIMARY KEY NOT NULL IDENTITY(1,1),
idusuario int FOREIGN KEY REFERENCES usuario(id),
idvehiculo int FOREIGN KEY REFERENCES vehiculo(id),
fecha date NOT NULL
)
GO