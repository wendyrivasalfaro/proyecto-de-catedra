﻿
namespace ProyectoDeCatedra_POO
{
    partial class frmEliminarVehiculo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEliminarVehiculo));
            this.lboxVehiculos = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lboxVehiculos
            // 
            this.lboxVehiculos.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lboxVehiculos.FormattingEnabled = true;
            this.lboxVehiculos.ItemHeight = 21;
            this.lboxVehiculos.Location = new System.Drawing.Point(116, 108);
            this.lboxVehiculos.Name = "lboxVehiculos";
            this.lboxVehiculos.Size = new System.Drawing.Size(332, 277);
            this.lboxVehiculos.TabIndex = 36;
            this.lboxVehiculos.SelectedIndexChanged += new System.EventHandler(this.lboxVehiculos_SelectedIndexChanged);
            this.lboxVehiculos.DoubleClick += new System.EventHandler(this.lboxVehiculos_DoubleClick);
            this.lboxVehiculos.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lboxVehiculos_MouseDoubleClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Impact", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.Color.DarkRed;
            this.label7.Location = new System.Drawing.Point(195, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 27);
            this.label7.TabIndex = 35;
            this.label7.Text = "Eliminar vehiculo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(500, 19);
            this.label1.TabIndex = 34;
            this.label1.Text = "Haga doble click en el vehiculo que desea retirar del catalogo:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 9);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(78, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 37;
            this.pictureBox2.TabStop = false;
            // 
            // frmEliminarVehiculo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(578, 427);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lboxVehiculos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Name = "frmEliminarVehiculo";
            this.Text = "frmEliminarVehiculo";
            this.Load += new System.EventHandler(this.frmEliminarVehiculo_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lboxVehiculos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}