﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace ProyectoDeCatedra_POO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            
            SqlCommand comando = new SqlCommand("SELECT tipo FROM usuario where usuario = @USUARIO and password = @PASSWORD", BDD.conexion);
            comando.Parameters.AddWithValue("@USUARIO", txtUsuario.Text);
            comando.Parameters.AddWithValue("@PASSWORD", txtPassword.Text);
            BDD.open();
            SqlDataReader consulta = comando.ExecuteReader();
            if (consulta.Read())
            {
                    this.Hide();
                    if((consulta["tipo"].ToString() == "administrador")){
                        BDD.close();
                        frmAdministracion frm = new frmAdministracion();
                        frm.Show();
                    }
                    else
                    {
                        BDD.close();
                        frmVehiculos frm = new frmVehiculos();
                        frm.Show();
                    }
            }
            else
            {
                MessageBox.Show("Credenciales incorrectas. Intente de nuevo", "Error de inicio de sesion");
                BDD.close();
            }
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmRegistro form = new frmRegistro();
            form.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
