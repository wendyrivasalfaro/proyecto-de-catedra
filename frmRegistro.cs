﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProyectoDeCatedra_POO
{
    public partial class frmRegistro : Form
    {
        public frmRegistro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtNombre.Text) || String.IsNullOrWhiteSpace(txtUsuario.Text) || String.IsNullOrWhiteSpace(txtPassword.Text))
            {
                MessageBox.Show("Ingrese todos los datos", "Error");
            }
            else
            {
                SqlCommand comando = new SqlCommand("INSERT INTO usuario VALUES (@NOMBRE,@USUARIO,@PASSWORD,'cliente')", BDD.conexion);
                comando.Parameters.AddWithValue("@USUARIO", txtUsuario.Text);
                comando.Parameters.AddWithValue("@PASSWORD", txtPassword.Text);
                comando.Parameters.AddWithValue("@NOMBRE", txtNombre.Text);
                BDD.open();
                int i = comando.ExecuteNonQuery();
                if (i!=0)
                {
                    MessageBox.Show("Usuario registrado con exito");
                    this.Hide();
                    Form1 form = new Form1();
                    form.Show();
                }
                else
                {
                    MessageBox.Show("Error en el registro. Contacte al administrador", "Error");
                }
                BDD.close();
            }
        }

        private void frmRegistro_Load(object sender, EventArgs e)
        {

        }
    }
}
