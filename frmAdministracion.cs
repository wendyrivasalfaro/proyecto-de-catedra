﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProyectoDeCatedra_POO
{
    public partial class frmAdministracion : Form
    {
        public frmAdministracion()
        {
            InitializeComponent();
        }

        private void frmAdministracion_Load(object sender, EventArgs e)
        {
            string consulta = "SELECT usuario as Usuario, nombre as Cliente, CONCAT(marca,' ',modelo,' ',anio) as Vehiculo, precio as Monto, fecha as Fecha FROM venta INNER JOIN usuario ON venta.idusuario=usuario.id INNER JOIN vehiculo ON venta.idvehiculo=vehiculo.id";
            SqlDataAdapter adaptador = new SqlDataAdapter(consulta, BDD.conexion);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            dgvVentas.DataSource = tabla;
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 frm = new Form1();
            frm.Show();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAgregarVehiculo frm = new frmAgregarVehiculo();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 frm = new Form1();
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmEliminarVehiculo frm = new frmEliminarVehiculo();
            frm.Show();
        }
    }
}
