﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms; 

namespace ProyectoDeCatedra_POO
{
    public partial class frmVehiculos : Form
    {
        int id;
        public frmVehiculos()
        {
            InitializeComponent();

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string foto;
            foto = dgvVehiculos.SelectedCells[6].Value.ToString();
            picVehiculo.ImageLocation = foto;
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmVehiculos_Load(object sender, EventArgs e)
        {
            
            string consulta = "select id,marca,modelo,anio,precio,stock,url from vehiculo";
            //string consultaurl = "select url from vehiculo";

            SqlDataAdapter adaptador = new SqlDataAdapter(consulta, BDD.conexion);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            dgvVehiculos.DataSource = tabla;

            dgvVehiculos.Columns[0].Visible = false;
            dgvVehiculos.Columns[6].Visible = false;
           

        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            
            //string nombre, marca, modelo, year, precio;

            frmVenta venta = new frmVenta();
            venta.txtMarca.Text = dgvVehiculos.SelectedCells[1].Value.ToString();
            venta.txtModelo.Text = dgvVehiculos.SelectedCells[2].Value.ToString();
            venta.txtYear.Text = dgvVehiculos.SelectedCells[3].Value.ToString();
            venta.txtPrecio.Text = dgvVehiculos.SelectedCells[4].Value.ToString();
            id = int.Parse(dgvVehiculos.SelectedCells[0].Value.ToString());

            this.Hide();
            venta.Show();
        }
    }
}
